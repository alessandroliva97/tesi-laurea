\select@language {italian}
\select@language {italian}
\contentsline {chapter}{\numberline {1}Introduzione}{9}
\contentsline {section}{\numberline {1.1}Timespot e Arcadia}{11}
\contentsline {section}{\numberline {1.2}Scopo del progetto}{12}
\contentsline {chapter}{\numberline {2}Teoria preliminare}{13}
\contentsline {section}{\numberline {2.1}Sensore a pixel}{13}
\contentsline {section}{\numberline {2.2}DMA}{16}
\contentsline {section}{\numberline {2.3}Struttura base di un sistema Embedded Linux}{18}
\contentsline {section}{\numberline {2.4}Il protocollo AXI}{19}
\contentsline {chapter}{\numberline {3}FPGA: ZYBO Zynq-7000 Development Board}{25}
\contentsline {section}{\numberline {3.1}Il sistema Zynq e il supporto AXI4}{27}
\contentsline {section}{\numberline {3.2}Programmazione FPGA}{30}
\contentsline {chapter}{\numberline {4}Analisi e Design}{31}
\contentsline {section}{\numberline {4.1}Specifiche}{31}
\contentsline {section}{\numberline {4.2}Modello e progettazione}{31}
\contentsline {subsection}{\numberline {4.2.1}Trasferimento dati}{32}
\contentsline {subsection}{\numberline {4.2.2}Comunicazione}{32}
\contentsline {section}{\numberline {4.3}Strumenti di sviluppo}{33}
\contentsline {subsection}{\numberline {4.3.1}Xilinx Vivado}{33}
\contentsline {subsection}{\numberline {4.3.2}Petalinux Tools}{33}
\contentsline {chapter}{\numberline {5}Sviluppo e implementazione}{35}
\contentsline {section}{\numberline {5.1}Hardware Block Design con Vivado}{36}
\contentsline {subsection}{\numberline {5.1.1}Il Process System}{36}
\contentsline {subsection}{\numberline {5.1.2}Il DMA}{37}
\contentsline {subsection}{\numberline {5.1.3}Il Loopback}{39}
\contentsline {subsection}{\numberline {5.1.4}Il Block design}{40}
\contentsline {section}{\numberline {5.2}Applicazione DAQFIFO}{43}
\contentsline {subsection}{\numberline {5.2.1}Comunicazione con altri device}{44}
\contentsline {subsection}{\numberline {5.2.2}Controllo del DMA}{44}
\contentsline {subsection}{\numberline {5.2.3}FIFO}{46}
\contentsline {subsection}{\numberline {5.2.4}Modulo per richiesta memoria}{46}
\contentsline {section}{\numberline {5.3}Embedded Linux con Petalinux}{46}
\contentsline {subsection}{\numberline {5.3.1}Configurazione del sistema}{47}
\contentsline {subsection}{\numberline {5.3.2}Generazione dei file di boot}{47}
\contentsline {section}{\numberline {5.4}Applicazione per il testing}{47}
\contentsline {subsection}{\numberline {5.4.1}Implementazione}{48}
\contentsline {chapter}{\numberline {6}Test, valutazione e calibrazione}{49}
\contentsline {section}{\numberline {6.1}Casi di test con Zybo}{49}
\contentsline {subsection}{\numberline {6.1.1}Caso A}{49}
\contentsline {subsection}{\numberline {6.1.2}Caso B}{53}
\contentsline {section}{\numberline {6.2}Calibrazione del sistema}{56}
\contentsline {chapter}{\numberline {7}Conclusioni}{57}
\contentsline {section}{\numberline {7.1}Sviluppo futuro}{57}
\contentsline {chapter}{Bibliografia}{59}
